<h1><b>VESTIGE</b></h1>
![Screenshot-miniex4.png](https://i.postimg.cc/LXM0nQ6G/Screenshot-miniex4.png)
<p>Everything is collected, critical information, minute details and pointless data. where does it go, why is it collected, what purpose does it serve to monitor and capture data and information to just let it bathe in the digital dust that builds up over time.</p>
<p>"VESTIGE" tries to show the absurdity of the collection of datapoints and visually show what is currently being processed live in the program. The talk of big data, and “big brother is watching” is all around us. But why does it matter, does this data turn useful instead of just being collected as part of a bureaucratized routine. And likewise, why do we care, is it because we want to rebel against it’s uses, its collection? What part of the process of data collection and usage are we against? “VESTIGE” confronts us with the reality, the scenery behind the curtain so to speak. The continuous cold robotic black and white fetching of data, important and unimportant, no discrimination.  
</p>
</p>

<h2>PROGRAM</h2>

<p>I initially wanted to showcase the absurdity and to some extent needlessness of data capture. To do this I wanted to separate the screen into 4 different segments. Two visual, and two text and data based. The first segment is a simple vertex that is being drawn and spliced to create a visual interpretation of the microphone input. The top right segment is a voice recognition library that interprets voice inputs and displays the “live” in text. This was done through the use of the p5.speech library made by R. Luke DuBois. It takes the audio input of the microphone and spits out a string result based on the precision of it’s recognition. I wanted this to be one of the main points of the program, to compliment the other datapoints and to signify another data collection point as “live” as is possible. </p>
<p>The bottom left segment is comprised of different “captures” among them, date, time, X and Y positions, microphone input amplitude and frames. While some of these datapoints seem non-important it is used to communicate what can be captured and what might seem useless to you, may be valuable for others. </p>
<p>The final segment is the webcam capture. This segment is quite simple as it is just a capture of your webcam, however it adds to the experience in the way that you are watching yourself, along with all the other data inputs that you as a user allow the program to detect. </p>
<p>As a whole this project gave quite a few insights, both into coding, but also what I myself allow to be captured. The words of Edward Snowden began to ring in the back of my head while creating this program <i>“Arguing that you don't care about the right to privacy because you have nothing to hide is no different than saying you don't care about free speech because you have nothing to say." "When you say, 'I have nothing to hide,' you're saying, 'I don't care about this right.” </i></p>
<p>Dystopic as it is, it scratches a feeling that could make one either scared, queasy, angry or apathetic. </p>

<h2>CAPTURE_IN_PROGGRESS</h2>
<p>In the most literal sense, the program attempts to “capture all”. Capturing as much information as possible and displaying it rather than obscuring it to some extent. The thinking behind the program itself was to show this behind the scenes aspect, and to show what is being captured while on a simple site. The black and white imagery is to showcase the robotic and binary point of view, while the added camera is to act as some kind of mirror or perspective from the browsers own view. A way of getting a feel for what the computer may see and not just the human eyes. You could of course go on and on In terms of visualizing what is being captured, but being somewhat minimalistic yet descriptive in both numbers and visualization of these datapoints allow it to not be cluttered, and instead being readable and understandable for any user. </p>

<h2>IMPLICATIONS_OF _DATA </h2>

<p>I am quite ambivalent when it comes to the topic of data capture. While the practice of capturing, storing and coding data capture is fascinating to me the use of it is somewhat mystifying. The range of use can go from being simple and harmless, to complex and devastating. This duality of data capture is however also a part of the fascination that I hold. Without data capture many sites would become useless, but also much less infringing on the virtual personal space.</p>
<p>Without data capture marketing becomes bland, recommendations turn irrelevant, programs useless, and the list goes on for a while here. Data capture is essential, the use also. The interpretation and use of this data however need more ethical work. While data capture is a hotly debated topic, it is somehow still so invisible to us. Much like the bottom of the ocean, there’s life, there’s activity, but its hidden by the lack of light. And perhaps it wouldn’t have be the same if suddenly it all lighted up.</p>


[- RUN - ](https://adeve_.gitlab.io/ap2020/MiniEx_4/)  
[- CODE -](https://gitlab.com/Adeve_/ap2020/-/blob/master/public/MiniEx_4/MiniEx_4.js)
