
let capture;
var mic;

var volhistory = [];

let lang = navigator.language || "en-US"
let speechRec = new p5.SpeechRec("en-US", gotSpeech);
let continuous = true;
let interim = true;




function setup(){
  createCanvas(windowWidth, windowHeight);

  //VIDEO
    capture = createCapture(VIDEO);
    capture.size(windowWidth/2, windowHeight/2)
    capture.hide();
  //MIC
    mic = new p5.AudioIn();
    mic.start();
  //SPEECH
    speechRec.start(continuous, interim);
}


/////////////////////////////////////////////////
/////////////BIG PIMPIN DRAW FUNCTION////////////
/////////////////////////////////////////////////

function draw(){
  background(0)
  camerafeed();
  micfeed();
  gotSpeech();
  observations();
  currenttime();
  collected();
  infotext();
}
////////////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////

function camerafeed(){

  image(capture, windowWidth/2, windowHeight/2, windowWidth/2, windowHeight/2)
  //MAYBE KEEP
    //filter(THRESHOLD, 0.35)
  filter(GRAY)
}

function micfeed(){
 var vol = mic.getLevel();
 volhistory.push(vol);
    push();
     stroke(255)
      noFill();
     beginShape();
     for (var i = 0; i < volhistory.length; i++) {
       var y = map (volhistory[i], 0, 1, 270, 0);
       vertex(i, y);
     }
     endShape();
     noStroke();
    pop();

 if(volhistory.length > windowWidth/2){
   volhistory.splice(0,1);
 }
}

function gotSpeech(){
  if (speechRec.resultValue){
    push();
        textSize(20)
        fill(255)
        textAlign(CENTER)
        text(speechRec.resultString, windowWidth/1.35, windowWidth/8);

        console.log(speechRec.resultString);
    pop();
  }
}

function observations(){
  //amplitude of the microphone
    push();
      var vol = mic.getLevel();
      vol = round(vol*1000000)
      fill(255)
      textSize(30)
      text("MIC_INPUT: "+vol,windowWidth/40,windowHeight/1.1)
    pop();
  //X & Y coordinates
  push();
    fill(255)
    textSize(30)
    text("X POSITION: "+ mouseX, windowWidth/40, windowHeight/1.3)
    text("Y POSITION: "+ mouseY, windowWidth/40, windowHeight/1.25)
  pop();
}

function currenttime(){
  push();
    d = day()
    m = month()
    y = year()
    h = hour()
    mi = minute()
    se = second()

    fill(255)
    textSize(50)
    text("DATE: "+d + "/" + m + " - " + y, windowWidth/40, windowHeight/1.9)
    textSize(54)
    text("TIME: "+h+":"+mi+":"+se, windowWidth/40, windowHeight/1.7)
  pop();
}

function collected(){
  push();
    fill(255)
    textSize(30)
    text("FRAMES_COLLECTED: "+frameCount,windowWidth/40, windowHeight/1.47 )
  pop();
}

function infotext(){
  push();
    fill(255)
    textSize(30);
    textAlign(CENTER)
    text("VOICE_INPUT",windowWidth/1.34, windowWidth/30)



  pop();
}

function windowResized(){
  resizeCanvas(windowWidth, windowHeight)
}
