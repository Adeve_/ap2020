//the cardinal directions used in the "if" statement
var NORTH     = 0;
var NORTHEAST = 1;
var EAST      = 2;
var SOUTHEAST = 3;
var SOUTH     = 4;
var SOUTHWEST = 5;
var WEST      = 6;
var NORTHWEST = 7;

//the direction of the drawing, aswell as direction 2 to give a more non-linear movement
var direction =0;
var direction2 =2

//the change in movement and position and the size of the ellipse.
var stepSize = 3;
var diameter = 0.5;

//position of the "drawing" line
var posX;
var posY;

var counter = 0;

//Song data
var amp;

//Song preload
function preload(){
  song = loadSound("PIANO.wav")
}

function setup(){
  var cnv = createCanvas(windowWidth, windowHeight);
  cnv.style('display', 'block');
  song.play();
  background(0)
  amp = new p5.Amplitude();
  posX = width/2;
  posY = height/2;

}


function draw(){
  var vol = amp.getLevel();
  for (var i = 0; i <= vol*2000; i++){
  counter++;

//the nonlinear movement
  direction = int(random(7));
  direction = direction * direction2

//the conditional statement controlling direction of movement based on position.
  if (direction == NORTH) {
    posY -= stepSize;
  } else if (direction == NORTHEAST) {
    posX += stepSize;
    posY -= stepSize;
  } else if (direction == EAST) {
    posX += stepSize;
  } else if (direction == SOUTHEAST) {
    posX += stepSize;
    posY += stepSize;
  } else if (direction == SOUTH) {
    posY += stepSize;
  } else if (direction == SOUTHWEST) {
    posX -= stepSize;
    posY += stepSize;
  } else if (direction == WEST) {
    posX -= stepSize;
  } else if (direction == NORTHWEST) {
    posX -= stepSize;
    posY -= stepSize;
  }
//Edge check. to make sure if the drawing goes beyond the edges, it redraws in a corresponding spot on the opposite side of the canvas.
  if (posX > width) posX = 0;
  if (posX < 0) posX = width;
  if (posY < 0) posY = height;
  if (posY > height) posY = 0;

  stroke(random(255))
  ellipse(posX + stepSize / 2, posY + stepSize / 2, diameter, diameter);
  noFill();

    }
  }

//The function to "clear" the canvas of the drawing.
  function keyPressed() {
    if (keyCode === BACKSPACE) {
      background(0);
    }
}
