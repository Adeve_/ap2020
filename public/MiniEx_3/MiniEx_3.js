//circle reference for the lines to congregate
let distribution = new Array(720);
//move functions to determine values for start(move) and change(move2)
let move = 0
let move2 = 2
//loading 0-100%
let load = 0
let load2 = 1

//Song Variable
var song;

//Amplitude object - get amplitude values of song
var amp;
var volData = Array(360).fill(0)

//button for music
function toggleSong(){
  if (song.isPlaying()){
    song.pause();
  }else{
    song.play();
  }
}
//preload function to load up the song
function preload(){
  song = loadSound("TIMESINK.mp3");
}

function setup(){
  createCanvas(windowWidth, windowHeight)
  //button function
    button = createButton("SOUND")
    button.mousePressed(toggleSong)
  angleMode(DEGREES)
  amp = new p5.Amplitude();
  for (let i =0; i < distribution.length; i++){
    distribution[i] = floor(randomGaussian(500, 500));
  }
}

function draw(){
  background(0)
    //value of the songs amplitude.
    var vol = amp.getLevel();
    volData.push(vol);
    stroke(0,random(255),255)
    noFill();
  //circular frequency line.
    translate(width/2,height/2)
      push();
        beginShape();
          for (var i = 0; i < volData.length; i++){
            var r = map(volData[i], 0, 1, 10, 2000);
            var x = r * cos(i);
            var y = r * sin(i);
            vertex(x, y);
          }
        endShape();
      pop();
          if (volData.length > 360){
            volData.splice(0, 1);
          }
      noStroke();
  frameRate(60);
  fill(255, 255, 255)
  textSize(90)
  textAlign(CENTER);
  gaussian();
  //movement - if statement to determine when to move in and out.
    move = move +move2
      if (move > 400*2 || move < -400*2){
        move2 = move2 *-1
    }
    //loading illusion - affecting gaussian function.
    load = load +load2
      if (load > 720 || load < 0){
        load2 = load2 *-1
  }

}
//gaussian function for movement
function gaussian(){
  for (let i =load; i < distribution.length; i++){
    rotate(360 / distribution.length);
    stroke(255)
    let dist = abs(distribution[i]);
    line(0, 200, dist, move)

  }
//window resize function
function windowResized(){
  resizeCanvas(windowWidth, windowHeight);
}
//END BRACKET
}
