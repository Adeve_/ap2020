<h1>-PERPETUAL_MOTION-</h1>
![MiniEx-3.png](https://i.postimg.cc/ZqYwbZ7b/MiniEx-3.png)

<h2>-SKETCH_&_EXPRESSION-</h2>
<p>My sketch is supposed to go against the traditional throbber. I wanted to add random elements to mimic the entropy as well as the order of the throbber. While it is randomly generated, It all circles a circular point. To further bolster the idea of loading I removed and added the lines from the circle to create a constant back and forth. 
</p>
I wanted to express the lackluster parts of loading, they’re boring, but behind them are interesting tasks, whether it is loading a large dataset, or compressing files. And I wanted to have the throbber be at least somewhat of a mesmerizing experience rather that just something we wait for. By creating some sort of hypnotizing rhythm and repetition. Most throbbers are predictable and often derivative of each other. I believe that giving users a more visual experience is important, even in loading.

<h2>-TIME_&_FUNCTION-</h2>
<p>There are no definite “time-related” syntaxes so to speak. The perceived time comes from the framerate and the adding of values from every draw loop. I even tried to go against this perception of time and focused on almost “hypnotizing” the watcher. Using counter-movements in the “gaussian” function the circle appears to move and spin in different speeds and directions. To add to the effect of constant running in place, I added the song “TIMESINK” a song made from analog and human sounding changes. However, the changes are minute and the rhythms repetitive, yet still evolving. I used the amplitude of the song to visualize it’s repetition and add a bit of color when attention shifts are needed.<br  />
</p>

<p>In line with Femkes article on geometries and the point that I took away with finding geometries in uncommon places or objects I wanted to challenge the concept of what loading is, to show the range in which the loading process affects both near and far in the data hierarchy. Furthermore I thought it important to have all these lines intersect into a larger web inside of the loading circle to let the concept shot its interconnectedness. 
</p>


<h2>-CULTURE_LOADING-</h2>

<p>When I think about throbbers most of them are similar in style, but one specific throbber/loading graphic comes to mind. Usually progress of something is explained in 0-100% or from 0-360 degrees of completeness. But rather that going for the traditional text or visual style to represent loading, the FL Studio export progress bar uses different datapoints to contextualize progress. Even allowing the user to select their preferred style. The standard version of the throbber uses the amount of bars in the song/project and visualizes the progress of every bar. This way of showing a more direct correlation to loading or progress creates a better insight for the user rather than having all of this data rounded down to a range of 100%. 
</p>

<p><i>PS: Scroll down a bit and press the button to get the full experience</i>
</p>
[-RUN-](https://adeve_.gitlab.io/ap2020/MiniEx_3/)

[-CODE-](https://gitlab.com/Adeve_/ap2020/-/blob/master/public/MiniEx_3/MiniEx_3.js)




