function setup(){
  createCanvas(1280, 720)
  print("NOISE")
}
let noiseScale=0.02;

function draw() {
  background(0, 0, 0);
  ellipse(1280/2, 720/2, 555, 555);
  ellipse(1280/2, 720/4, 333, 333);
  for (let x=0; x < width; x++) {
    let noiseVal = noise((mouseX+x)*noiseScale, mouseY*noiseScale);
    stroke(noiseVal*255);
    line(x, mouseY+noiseVal*200, x, height);
fill(0);
  }
}
