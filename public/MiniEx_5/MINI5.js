var song;
var amp;


let noiseMax = 0
let zoff = 0;

function preload(){
  song = loadSound("ambient.mp3")
}

function setup(){
  createCanvas(windowWidth, windowHeight)
  song.play()
  amp = new p5.Amplitude();
}

function draw(){


  centerpiece(); //----> Contains translate
  perimiter();
  outerperimiter();
  centerpiece2();
  centereye();



}


function centerpiece(){
  background(0,20)
  translate(width/2, height/2)
  stroke(158, 255, 245);
  noFill();

  beginShape();

    for (let a = 0; a < TWO_PI; a+=0.01){
      let xoff = map(sin(a),-1, 1, 0, vol*1000)
      let yoff = map(cos(a),-1, 1, 0, vol*1000)
      let r = map(noise(xoff, yoff, zoff), 0, 1, 250, 500)
      let x = r* cos(a);
      let y = r* sin(a);

      vertex(x,y,1)

      zoff += 0.0001
      var vol = amp.getLevel();
  endShape(CLOSE);

  }
}

function centerpiece2(){
  background(0,50)
  stroke(255);
  noFill();

  beginShape();

    for (let a = 0; a < TWO_PI; a+=0.01){
      let xoff = map(sin(a),-1, 1, 0, vol*40)
      let yoff = map(cos(a),-1, 1, 0, vol*40)
      let r = map(noise(xoff, yoff, zoff), 0, 1, 100, 250)
      let x = r* cos(a);
      let y = r* sin(a);

      vertex(x,y,1)

      zoff += 0.00001
      var vol = amp.getLevel();
  endShape(CLOSE);

  }
}

function centereye(){
  background(0,50)
  stroke(255);
  noFill();

  beginShape();

    for (let a = 0; a < TWO_PI; a+=0.01){
      let xoff = map(sin(a),-1, 1, 0, vol*10)
      let yoff = map(cos(a),-1, 1, 0, vol*10)
      let r = map(noise(xoff, yoff, zoff), 0, 1, 50, 100)
      let x = r* cos(a);
      let y = r* sin(a);

      line(x,y,1,1)

      zoff += 0.00001
      var vol = amp.getLevel();
  endShape(CLOSE);

  }
}

function perimiter(){
  background(0,20)
  stroke(70, 225, 255);
  noFill();

  beginShape();

    for (let a = 0; a < TWO_PI; a+=0.01){
      let xoff = map(sin(a),-1, 1, 0, vol*1000)
      let yoff = map(cos(a),-1, 1, 0, vol*1000)
      let r = map(noise(xoff, yoff, zoff), 0, 1, 300, 700)
      let x = r* cos(a);
      let y = r* sin(a);

      vertex(x,y,1)

      zoff += 0.0001
      var vol = amp.getLevel();
  endShape(CLOSE);

  }
}

function outerperimiter(){
  background(0,20)
  stroke(103, 135, 132);
  noFill();

  beginShape();

    for (let a = 0; a < TWO_PI; a+=0.01){
      let xoff = map(sin(a),-1, 1, 0, vol*1000)
      let yoff = map(cos(a),-1, 1, 0, vol*1000)
      let r = map(noise(xoff, yoff, zoff), 0, 1, 400, 900)
      let x = r* cos(a);
      let y = r* sin(a);

      vertex(x,y,1)

      zoff += 0.0001
      var vol = amp.getLevel();
  endShape(CLOSE);

  }
}
