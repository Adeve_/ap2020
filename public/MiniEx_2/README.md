[![Screenshot-miniex2.png](https://i.postimg.cc/FRqGcfP8/Screenshot-miniex2.png)](https://postimg.cc/xJvKVdV3)

Describe your program and what you have used and learnt.
<h2> Everchanging mO_Ods </h2>
The program that I have written is inspired by both the multi project by David Reinfurt, and by the overcomplication and representation of emojis. 
The program depicts the first "emoji" which is an everchanging emoji with different types of eyes coming from an array of "eye-like" letters. these are randomly picked from this list, essentially creating 
a seemingly endless amount of alterations of the emojis possible. the mouth is static to somewhat mock the use of emojis as being a false mood or representation of what we feel when we use them. the program also contains an "if" statement that changes the layout of the program from a constantly changing emoji to a singular emoji with crossed out eyes that track the users mouse movements, as long as they're in the confines of the if statements parameters.
During the program's life, it saw many changes. from to two static emoji that would change based on cursor position, to what eventually became the more interesting randomly generated emoji.
specifically, the syntax in this particular program is important to maintain certain effects such as the jittery picture when the mouse is on the top half, as well as the smooth motion of the "x" eyes when on the bottom.
this was done through changing a variable throughout the code to have different settings and experiences.

<h2> I_N  F_O </h2>
As shortly mentioned above, the use of emojis seems to have shifted into mass adoption of different iconographies and visual types. although they may seem similar, they all serve somewhat of a purpose. however, this purpose
seems more and more watered down. the task of adding an emoji seems almost routine, making it less personal and more of a ritual rather than "need".
while emojis communicate specific feelings, they all become intertwined in different subcultures and suddenly their meanings change drastically from culture to culture. 

the goal was to create a program that showed, albeit simply. the dichotomy of the massive amounts of impersonalized emojis, hence the randomly generated emoji, and the routine aspect of emojis as represented by the solid "X_X" emoji.
while it may seem drastic to directly call the use of emojis needless, the use of them has slowly turned users apathetic to their meaning. 
Thus, I see the solution as not to be fixable by quantity, but rather of quality and personalization. because that is how the emoji found its footing early on. text-chains with meticulously crafted emojis that showcased finesse
on not just the designers end, but also on the users end. 
The act of assigning value to user created emojis seems relevant for the use of emojis to survive without drastic innovation of forced change.

RUN M_E:
https://adeve_.gitlab.io/ap2020/MiniEx_2/
