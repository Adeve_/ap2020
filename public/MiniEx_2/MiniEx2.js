//Definition of the framerate
let fr = 3

function setup(){
  createCanvas(720, 720,);
  frameRate(fr)
}
function draw(){
// Define mouse as a y value
  let hp = mouseY
//if statement to determine state of the program
  if (hp < 720/2) {
  //define framerate as 3 to create stutter between the random alterations
    fr = 3
    frameRate(fr)
  // Array for the different "eye" variables. allowing expression
    let eyeshape = ['O', 'U', 'Q', 'V', 'i', 'v', 'o', 'u', 'q', 'T', 't', '@' ]
    //Left eye
      let eyes = random(eyeshape)
    //Right eye
      let eyes2 = random(eyeshape)
        background(0)
        textSize(200);
        fill(255)
      //Eye variable inserted to replace plain string text
        text(eyes, 240, 200);
        text(eyes2, 240*2, 200);
        text("_", 720/2, 400);
        textAlign(CENTER)
      //framecounter to count the iterations of smileys
          textSize(30)
          text(frameCount, width / 2, height / 1.05);
            noFill()
// if the mouse value is >720/2 the "reset" or death screen appears
  }else {

    if (hp > 720/2){
    //Change frameRate back to 60 for a smoother animation of the "X X" eyes when moving the cursor.
      fr = 60
      frameRate(fr)
        background(255)
        textSize(200);
        fill(0)
          push();
            translate(mouseX*0.05, mouseY*0.05)
            text("X", 240, 200);
            text("X", 240*2, 200);
          pop();
        text("_", 720/2, 400);
        textAlign(CENTER)
          noFill()

}
}
}
