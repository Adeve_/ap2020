
let snake
let resolution = 60
let food;
let w;
let h;

function preload(){
  img = loadImage("test.gif")
  terminal = loadImage("terminal.png")
  endscreen = loadImage("endscreen.png")
}

function setup(){
  var cnv = createCanvas(windowWidth, windowHeight)
  cnv.style('display', 'block')

  w =floor(width/resolution)
  h= floor(height/resolution)
  frameRate(10);
    snake = new Snake();
  foodLocation();
}

function foodLocation(){
  let x = floor(random(w))
  let y = floor(random(h))
  food = createVector(x, y)
}
function draw(){
  scale(resolution);
  image(terminal,0,0, windowWidth/resolution,windowHeight/resolution)
  if (snake.eat(food)){
    foodLocation();
  }
  snake.eat(food);
  snake.update();
  snake.show();

//MAKE FUNCTION
  if (snake.death()){
    print("DEAD")
    image(endscreen,0,0, windowWidth/resolution,windowHeight/resolution)
    noLoop();
  }

  noStroke();
  fill(255, 0, 0)

  image(img, food.x, food.y, 1, 1 )
}

function keyPressed(){
if (keyCode === LEFT_ARROW){
  snake.setDir(-1,0);
} else if (keyCode === RIGHT_ARROW) {
  snake.setDir(1,0);
} else if (keyCode === UP_ARROW) {
  snake.setDir(0,-1);
} else if (keyCode === DOWN_ARROW) {
  snake.setDir(0,1);
}
}
