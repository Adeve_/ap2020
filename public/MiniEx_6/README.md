![Screenshot-miniex6.png](https://i.postimg.cc/mrY1S7hj/Screenshot-miniex6.png)

[RUN](https://adeve_.gitlab.io/ap2020/MiniEx_6/)

[CODE](https://gitlab.com/Adeve_/ap2020/-/blob/master/public/MiniEx_6/MiniEx6.js)  
[CLASS](https://gitlab.com/Adeve_/ap2020/-/blob/master/public/MiniEx_6/Snake.js)


<h2>The Snake And The Food</h2>
<p>The game that I’ve programmed for this MiniEx is an interpretation of the classic game of snake as we know it. The controls and goals are simple. You are tasked to chase/eat some sort of object while maneuvering the snake in one of 4 directions. As an added layer to the game I’ve decided to go for an aesthetic that is reminiscent of older terminals with a green tint to it. And instead of the classic red fruit, a virus that the snake is supposed to get rid of. Along these lines as an end-game state I added a green skull along with accompanying text to showcase the consequence of hitting either one of the walls or bumping into yourself. There is no particular goal other than to keep going for as long as you can manage. Thinking back on the program, perhaps adding elements of score, precise goal, or increasing difficulty would have made for a more entertaining playthrough.
</p>
<h2>How does it work?</h2>
<p>
The snake object consists of quite a few noticeable parts. Mainly the body which acts as the main operator. And the “xdir” “ydir” to decide the direction of movement upon keypresses. Furthermore, we have the growth function. Copying the array of the body, and adding another onto the last position using “this.body.push(head)”. The death function is somewhat simple. It is setup to return true if the head of the snake collides with either one of the walls, or of it collides with a part of its body. </p>
<p>Whenever the snake eats a piece of food two things are triggered within the class, the first being the eat function which determines the collision of the head and check to see if the x or y values match the corresponding food location. If this returns as true, the snake grows in size due to the grow function triggering. 
</p>
<h2>Object oriented programming</h2>
<p>
When talking about object-oriented programming, there are main points that matter. For example, the part of encapsulation, helping reduce the complexity of the code in its entirety. Removing the dependency from functions and avoiding writing spaghetti code, thus also increase the reusability of the code itself. Furthermore, using the point of abstraction to further decrease the complexity, hiding the chaos of code from the user, making interfaces simpler, or codes more understandable, a nice way to put it is as if you were interacting with a product with buttons. These buttons do something that they are assigned to do, but users often don’t care about the logic and execution behind every button press. </p>
<p>Lastly it helps in terms of removing redundant code in a more out of sight way. Allowing the programmer to freely manipulate function and objects without having huge changes in the program itself. Freeing up the code to make it more readable and easier to navigate. 

</p>
<h2>The daily life of an object</h2>
<p>
When I think of objects and the way they work I assign it to the way I interact with a software synthesizer, the knobs and button act as gates to different objects or classes within, while they are not visible they are instead audible. So if I were to for example turn on the distortion and filter effects inside of the program they act as different classes that affect the program itself. I see the simple sine wave output as the program running and the added classes/objects, or in this case distortion and filter as the added variables that come to light when interacted with. I don’t see the logical code execution behind their processing but instead it acts as a hidden agent, modifying and improving, or at the very least changing the program in some way. And the same concept goes for a lot of hardware technology as well. If we take a DVD player as an example, it might have different buttons for pause, skip, eject disk or record. These I see as objects or lines of code that execute. But they are not necessarily part of the main program. Instead they are additions to further allow and simplify interactions with the program or DVD player as a whole. 
</p>
