<h2>My First Experience in Programming</h2>
I am used to working with more graphical work and therefore also much more visual tools. However, 
during this coding experience a lot more thought was on thinking about the logical and chronological way of thinking and arranging different elements. in some way it drew 
correlations to how to set up layers in an image or texturing sound with different modified samples. but instead of visual images or waveforms, it is replaced by text.
And as you would with sound or images, manipulating the different values yielded different results. And in this first experience it was a lot more trial and error
rather than objectively knowing what to do. this allowed me to move back and forth between ideas and think of different ways to write the particular code.

<h2>The similarities and descrepancies</h2>
Reading and writing is a lot more freeform than what we may assume, we can draw logical conclusions to sentences even if the syntax is incorrect or even gibberish
however when writing code this is crucial for the computer to understand how and when to process the writing. There is a heavy emphasis on writing punctually and correctly
unless you want to garner no results. and as such a lot more thought has to go into "speaking" the so called language.

<h2>The story behind</h2>
So far, in my own exploration of coding i have been purely looking at how to write specific code and achieve a specific result. Not a lot of thought had gone into why or how 
these lines of code came to, changed elements or how they impacted the world around us. Reading the litterature gave a more broad insight into both the history and effects of coding
and how it was used throughout parts of history. having this knowledge allows me to think alittle deeper than the surface level of coding and hopefully down the line achieve some sort
of higher goal, whether social or personal, than to just blindly code without thought to the world around you and the implications a program might have.

<h1>The Code</h1>
![SCREENSHOT](https://i.postimg.cc/pdX2hByF/Udklip.png)